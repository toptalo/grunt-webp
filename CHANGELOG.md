# Release History

## v0.5.0, 2020-08-04
* update `dependencies`
* update `devDependencies`

---

## v0.4.0, 2019-12-13
* change log messages

---

## v0.3.0, 2019-12-06
* add `png` and `jpeg` options

---

## v0.2.0, 2019-12-05
* add `lossless` option

---

## v0.1.0, 2019-12-05
* defined task
