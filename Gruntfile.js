/*
 * @toptalo/grunt-webp
 * https://gitlab.com/toptalo/grunt-webp
 *
 * Copyright (c) 2019 Виктор Виктор
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        clean: {
            tests: ['tmp']
        },

        cwebp: {
            options: {
                quality: 65,
                png: {
                    lossless: true
                }
            },
            files: {
                expand: true,
                cwd: './test/images/',
                src: ['**/*.{png,jpeg,jpg}'],
                dest: './tmp/',
                ext: '.webp'
            }
        }
    });

    // Actually load this plugin's task(s).
    grunt.loadTasks('tasks');

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-clean');

    // Whenever the "test" task is run, first clean the "tmp" dir, then run this
    // plugin's task(s), then test the result.
    grunt.registerTask('test', ['clean', 'cwebp']);

    // By default, lint and run all tests.
    grunt.registerTask('default', ['test']);

};
