# @toptalo/grunt-webp [![pipeline status](https://gitlab.com/toptalo/grunt-webp/badges/master/pipeline.svg)](https://gitlab.com/toptalo/grunt-webp/commits/master)

Grunt plugin to convert images to WebP format

Build upon [node-webp](https://github.com/Intervox/node-webp), Node.js wrapper for cwebp and dwebp binaries from WebP image processing utility.

## Getting Started
This plugin requires Grunt `^1.0.1`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install @toptalo/grunt-webp --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('cwebp');
```

## The "cwebp" task

### Overview
In your project's Gruntfile, add a section named `cwebp` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
  cwebp: {
    options: {
      // Task-specific options go here.
    },
    your_target: {
      // Target-specific file lists and/or options go here.
    }
  }
});
```

### Options

#### options.quality
Type: `Number`

Quality factor (0:small..100:big), default=75

### Usage Examples

```js
grunt.initConfig({
    cwebp: {
        options: {
            quality: 75,
            png: {
                lossless: true
            }
        },
        files: {
            expand: true,
            cwd: './test/images/',
            src: ['**/*.{png,jpeg,jpg}'],
            dest: './tmp/',
            ext: '.webp'
        }
    }
});
```

## Contributing
In lieu of a formal style guide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

## Sponsored by

[![DesignDepot](https://designdepot.ru/static/core/img/logo.png)](https://designdepot.ru/?utm_source=web&utm_medium=npm&utm_campaign=grunt-webp)

## Release History
See the [CHANGELOG](CHANGELOG.md).
