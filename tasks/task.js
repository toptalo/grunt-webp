/*
 * @toptalo/grunt-webp
 * https://gitlab.com/toptalo/grunt-webp
 *
 * Copyright (c) 2019 Виктор Виктор
 * Licensed under the MIT license.
 */

'use strict';

const {statSync} = require('fs');
const {CWebp: cWebp} = require('cwebp');
const chalk = require('chalk');
const plur = require('plur');
const prettyBytes = require('pretty-bytes');

module.exports = (grunt) => {
    grunt.registerMultiTask('cwebp', 'Convert images to WebP format', function () {
        const done = this.async();
        const options = this.options({
            quality: 95
        });
        const total = {
            amount: 0,
            sizeBefore: 0,
            sizeAfter: 0
        };

        function getFileSize (filePath) {
            return statSync(filePath)['size'];
        }

        function prettyPercent (percent) {
            return `${percent.toFixed(1).replace(/\.0$/, '')}%`;
        }

        Promise.all(this.files.map((file) => {
            return new Promise((resolveMain, rejectMain) => {
                Promise.all(file.src.filter((filePath) => {
                    if (!grunt.file.exists(filePath)) {
                        grunt.warn(`Source file ${filePath} not found.`);
                        return false;
                    } else {
                        return getFileSize(filePath) !== 0;
                    }
                }).map((filePath) => {
                    return new Promise((resolveFile, rejectFile) => {
                        const encoder = cWebp(filePath);
                        const isJPEG = filePath.match(/\.jpe?g$/i);
                        const isPNG = filePath.match(/\.png$/i);
                        const params = Object.assign({}, options, isJPEG && options.jpeg ? options.jpeg : isPNG && options.png ? options.png : {});
                        const sizeBefore = getFileSize(filePath);

                        if (params.lossless) {
                            encoder.lossless();
                        }
                        if (params.quality) {
                            encoder.quality(params.quality);
                        }

                        encoder.toBuffer().then((buffer) => {
                            grunt.file.write(file.dest, buffer);
                            const sizeAfter = getFileSize(file.dest);
                            const sizeDiff = sizeBefore - sizeAfter;
                            const diffPercent = (sizeDiff / sizeBefore) * 100;

                            total.amount += 1;
                            total.sizeBefore += sizeBefore;
                            total.sizeAfter += sizeAfter;

                            const msg = `saved ${prettyBytes(sizeDiff)} - ${prettyPercent(diffPercent)}`;
                            grunt.verbose.writeln(chalk.green('✔ ') + file.dest + chalk.gray(` (${msg})`));

                            resolveFile();
                        }).catch(error => {
                            grunt.log.error(error.message);
                            resolveFile(error);
                        });
                    });
                })).then(() => {
                    resolveMain();
                }).catch((error) => {
                    rejectMain(error);
                    grunt.fail.fatal(error);
                });
            });
        })).then((result) => {
            const totalSizeDiff = total.sizeBefore - total.sizeAfter;
            const totalDiffPercent = (totalSizeDiff / total.sizeBefore) * 100;

            let msg = `Converted ${total.amount} ${plur('image', total.amount)}`;

            if (total.amount > 0) {
                msg += chalk.gray(` (saved ${prettyBytes(totalSizeDiff)} - ${prettyPercent(totalDiffPercent)})`);
            }

            grunt.log.writeln(msg);
            done();
        }).catch((error) => {
            grunt.fail.fatal(error);
        });
    });
};
